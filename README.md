# What I did

I visited the google link in my browser.  It forwarded me to a different URL:

http://letsrevolutionizetesting.com

Which asked me to try JSON.  I figured it meant to send an Accept HTTP header
(I have used many REST APIs which refuse to respond unless this header is
sent).  I started writing a simple perl program (see `rf.pl`) to make this
easier.

Once I set the accept header and sent a GET request to the URL above, I got
back an HTML dump with an error message saying that cloudfare didn't like my
browser signature.  I figured it wanted something that looked more like a
"real" web browser so I just sent a Mozilla/5.0 user agent string.  I thought
it wouldn't accept this (since it's not really a "real" one) but it seemed
to.

Note that LWP::UserAgent follows redirects automatically.

The next thing I got was an HTML/JS dump which I redirected to a file and
opened in Firefox.  It was a page about the Rainforest QA culture and job
openings.  See `result.html` in this repository.

I called this script and got the resulting file like this:

    perl rf.pl > result.html

Thank you for reading!  I know my resume doesn't contain any Ruby/Rails
experience but I have quite a bit of backend experience in various
technologies and I have been working on remote teams for many years so
I am confident I could get up to speed and start contributing quickly.

-Brian

