use warnings;
use strict;

use Data::Dumper;
use LWP::UserAgent;

my $ua = LWP::UserAgent->new();

$ua->default_header('Accept', 'application/json'); # try json
$ua->agent('Mozilla/5.0'); # cloudfare error, doesn't like my browser >:(

my $res = $ua->get('http://letsrevolutionizetesting.com');

if($res->is_success()) {
    print $res->decoded_content();
}
else {
    print $res->status_line();
    print $res->content();
}

